(function($){

	$(document).ready(function(){

		webApp = new boilerplate('webApp');

		if(!webApp.ajaxer){
			webApp.start({
				singlePage: false,
				defaultPage: 'home',
				RewriteBase: "/",
				themeFolderName: 'cementleague',
				typekitID: 'hyh2grj',
				filesToVariablesArray: [
					{'page_home': 'views/page_home.php'},
					{'page_inner': 'views/page_inner.php'},
					{'blog_item': 'views/output_blog_item.php'},
					{'output_person_item': 'views/output_person_item.php'},
					{'output_person_category': 'views/output_person_category.php'},
					{'page_people_listing': 'views/page_people_listing.php'},
					{'output_union_category': 'views/output_union_category.php'},
					{'output_union_item': 'views/output_union_item.php'},
					{'page_news_listing': 'views/page_news_listing.php'},
					{'output_news_item': 'views/output_news_item.php'}
				],
				viewRender: {				
					"news": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){

							var thisPageData = that.accessor_listPages[pageID];
							var thisPageTitle = _.unescape(thisPageData.the_title)
							var thisPageObject = that.renderModel(thisPageData, $(that.php_page_news_listing));

							$('.header_title').html(thisPageTitle)
							$('.pageSection').html(thisPageObject);

							that.renderBannerSlideshow($('.bannerContainer'));
							that.renderNewsListing($('.news_listing_container'));
							that.changePage();

						});
					},					
					"unions-rates": function(thisThat){

						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){

							var thisPageData = that.accessor_listPages[pageID];
							var thisPageTitle = _.unescape(thisPageData.the_title)
							var thisPageObject = that.renderModel(thisPageData, $(that.php_page_inner));

							$('.header_title').html(thisPageTitle)
							$('.pageSection').html(thisPageObject);

							that.renderBannerSlideshow($('.bannerContainer'));							
							that.renderUnions($(".leftContainer"));							
							that.changePage();

						});

					},
					"leadership": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){

							var thisPageData = that.accessor_listPages[pageID];
							var thisPageTitle = _.unescape(thisPageData.the_title)
							var thisPageObject = that.renderModel(thisPageData, $(that.php_page_people_listing));

							$('.header_title').html(thisPageTitle)
							$('.pageSection').html(thisPageObject);

							that.renderBannerSlideshow($('.bannerContainer'));							
							that.renderPeopleListing($('.people_listing_container'));					
							that.changePage();

						});
					},					
					"home": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){

							var thisPageData = that.accessor_listPages[pageID];
							var thisPageTitle = _.unescape(thisPageData.second_editor);
							var thisPageObject = that.renderModel(thisPageData, $(that.php_page_home));

							$('.header_title').html(thisPageTitle)
							$('.pageSection').html(thisPageObject);

							that.renderBannerSlideshow($('.bannerContainer'));							
							that.changePage();

						});
					},
					"default": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){

							var thisPageData = that.accessor_listPages[pageID];
							var thisPageTitle = _.unescape(thisPageData.the_title)
							var thisPageObject = that.renderModel(thisPageData, $(that.php_page_inner));

							$('.header_title').html(thisPageTitle)
							$('.pageSection').html(thisPageObject);

							that.renderBannerSlideshow($('.bannerContainer'));							
							that.changePage();

						});
					}
					// FOR BLOG RENDERING
					// "blog": function(thisThat){
					// 	var that = thisThat;
					// 	that.renderBlog();
					// }
					// FOR SINGLE PAGE RENDERING
					// "default": function(thisThat, pageIDrequest){
					// 	var that = thisThat;
					// 	that.returnAndSaveJsonData('listPages', function(pagesData){
					// 		thisPageData = that.accessor_listPages[pageIDrequest];
					// 		pageObject = that.renderModel(thisPageData, $(that.php_page_inner));
					// 		pageObject.attr('id', 'page_' + pageIDrequest)
					// 		$('.pageSection').append(pageObject);
					// 	});
					// }
				},
				helpers: {
					renderNewsListing: function(target){
						var that = this;

						var views = {
							'news_item': that.php_output_news_item
						}

						var get_data = function(callback){
							that.returnAndSaveJsonData('listNewsItems',function(newsItemsData){
								console.log(newsItemsData)

								callback();
							})						
						}

						var build_dom = function(){
							var newsItemsData = that.jsonDataCollection['listNewsItems'];


							_.each(newsItemsData, function(value_newsData, index_newsData){
								var newsObject = that.renderModel(value_newsData, $(views.news_item))
								
								target.append(newsObject)

								newsObject.find('.news_date').html(moment(value_newsData.news_date, "X").tz("America/New_York").format('MMMM Do YYYY') );
							})
						}

						var init = function(){
							get_data(function(){
								build_dom();
							})
						}
						init();

					},					
					renderUnions: function(target){

						var that  = this;
						var target = (typeof target === "undefined") ? $('body') : target;
						var data  = {};
						var views = {
							unionTypeView: that.php_output_union_category,
							unionListView: that.php_output_union_item
						};

						var getUnonData = function(c){

							that.returnAndSaveJsonData("listUnionTypes", function(listUnionTypes){
								that.returnAndSaveJsonData("listUnions", function(listUnions){
									data.unionTypes = listUnionTypes;
									data.unions 	= listUnions;
									c();
								});
							});
							
						};
						var buildUnionsDOM = function(){

							$.each(data.unionTypes, function(unionTypes_index, unionTypes_value){
								var unionTypesId = unionTypes_value.post_id;
								var unionTypesView = that.renderModel(unionTypes_value, $(views.unionTypeView));
								target.append(unionTypesView);
								unionTypesView.addClass("unionTypes_" + unionTypesId);
							});

							$.each(data.unions, function(unions_index, unions_value){
								var unionItemView = that.renderModel(unions_value, $(views.unionListView));
								var unionType = unions_value.union_type;
								target.find(".unionTypes_" + unionType).find(".union_category").append(unionItemView);
							});

						};

						var init = function(){
							getUnonData(buildUnionsDOM);
						};
						init();
					},
					multiLevelPushMenu: function(target, position) {

						position = (typeof position === "undefined") ? 'right' : position;

						targetClone = target;
						targetClone.find('*').removeAttr('class')
						targetClone.removeAttr('class')

						targetClone.mmenu({
							autoHeight: true,
							offCanvas: {
								position: position
							},
							onClick: {
								close: function() {
									return ($(this).attr('data-pageid') != "");
								}
							}
						}, {
							clone: true
						});

						$('.mm-page').append($('.bannerContainer'))
						$('.mm-page').append($('.title_block'))
						$('.mm-page').append($('.pageSection'))
						$('.mm-page').append($('.footer'))

						$('.navbar-toggle').on('click', function(e){
							if($('#mm-navbar').hasClass('mm-opened')){
								$("#mm-navbar").trigger("close.mm");
							} else {
								$("#mm-navbar").trigger("open.mm");
							}
						});

					},
					renderPeopleListing: function(target){
						var that = this;

						var views = {
							'person_item': that.php_output_person_item,
							'category_item': that.php_output_person_category
						}

						var get_data = function(callback){
							that.returnAndSaveJsonData('listPeopleTypes', function(peopleType){
								that.returnAndSaveJsonData('listPeople', function(peopleData){
									callback();
								});
							})
						}

						var build_dom = function(){
							var peopleData = that.jsonDataCollection['listPeople']
							var peopleType = that.jsonDataCollection['listPeopleTypes']
							var default_image = '/wp-content/themes/cementleague/images/graphics/logo.png';
							

								_.each(peopleType, function(val_type, index_cat){
									var personTypeObj = that.renderModel(val_type, $(views.category_item));
									personTypeID = val_type.post_id;
									personTypeObj.addClass('personType' + personTypeID);

									target.append(personTypeObj)



								});

							_.each(peopleData, function(value_peopleData, index_peopleData){
									var personObject = that.renderModel(value_peopleData, $(views.person_item))
									var personTypeObj = $('.personType' + value_peopleData.person_type);
									personTypeObj.find('.peopleContainer').append(personObject)

									if(value_peopleData.featuredImage != null){
										personObject.find('.image_container').backstretch(value_peopleData.featuredImage);
									}
									else {
										personObject.find('.image_container').backstretch(default_image);
									}
							});
							
						}

						var init = function(){
							get_data(function(){
								build_dom();
							})
						}
						init();

					},
					bindMenuAnimations: function(menuTarget){
						menuTarget.children('li').mouseenter(function(){
							var hoverElement = $(this);
							var animationElement = hoverElement.children('ul');

							if(animationElement.size() > 0){

								dynamics.css(animationElement[0], {
									opacity: 0,
									display: 'block',
									scale: 1.1
								})
								dynamics.animate(animationElement[0], {
									scale: 1,
									opacity: 1
								}, {
									change: function(){
										if(typeof hoverElement.firstRun === "undefined" || hoverElement.firstRun == 0){
											hoverElement.firstRun = 1;
											hoverElement.siblings('li').children('ul').css('display','none');

											hoverElement.children('ul').children('li').each(function(index){
												var item = $(this)[0]
												dynamics.css(item, {
													opacity: 0,
													translateY: 20
												})

												dynamics.animate(item, {
													opacity: 1,
													translateY: 0
												}, {
													type: dynamics.spring,
													frequency: 300,
													friction: 435,
													duration: 1000,
													delay: 100 + index * 40
												})
											})

										}
									},
									complete: function(){
									},
									duration: 1000,
									type: dynamics.spring,
									frequency: 300,
									friction: 900,
								})
							}
						}).mouseleave(function(){
							var hoverElement = $(this);
							var animationElement = hoverElement.children('ul');

							if(animationElement.size() > 0){
								dynamics.animate(animationElement[0], {
									opacity: 0
								}, {
									complete: function(){
										animationElement.css('display', 'none');
									},
									delay: 300,
									duration: 300
								})
							}
						});

					},
					renderBannerSlideshow: function (target) {
						var that = this;
						var default_image;
						var image_for_banner = [];
						var thisPageData;
						var homePageData;

						var setImages = function (callback) {
							default_image = '/wp-content/themes/cementleague/images/graphics/logo.png';
							var imageArrayRequest = thisPageData.page_featured_images.split(',');
							// check if featured images are set
							if(imageArrayRequest.length == 1){
								if(imageArrayRequest[0] == null || imageArrayRequest[0] == ""){
									image_for_banner = [];
									image_for_banner.push(default_image)
								}

							}
							$.post(that.pageDir + "/machines/handlers/loadPost.php", {postRequest: imageArrayRequest}, function(featuredImagesData){
								_.each(imageArrayRequest, function(value_imageArrayRequest, index_imageArrayRequest){
									image_for_banner.push(featuredImagesData[value_imageArrayRequest].image_data.full)
								})


								callback()
							}, 'json');
						};

						var get_data = function (callback) {
							that.returnAndSaveJsonData('listPages', function (argument) {
								thisPageData = that.accessor_listPages[pageID];
								homePageData = that.accessor_listPages['home'];
								setImages(function(){
									callback();
								});
							})
						};

						var init = function (argument) {
							get_data(function (argument) {
								target.backstretch(image_for_banner, {
									fade: 200
								})
							})
						}
						init();
					},						
					cleanUp: function(pageIDrequest, postIDrequest){
						var that = this;

						// if IE 8
						if(!($('html').hasClass('internet-explorer-8') || js_mobile == "true")){
							that.bindMenuAnimations($('#menu-main-menu'));
						}



						// if mobile device
						if(js_mobile == "true"){
							if($('#mm-navbar').size() == 0){
								that.multiLevelPushMenu(jQuery('#navbar'))
							}
							$("#mm-navbar").trigger("close.mm");
						}

						// page specific cleanups
						switch(pageIDrequest){
							default:
							break;
						}
					},
					adminPages: function(currentPage){
						var that = this;
						switch(currentPage){
							case "galleries":
								that.renderGalleriesAdmin();
							break;

							case "page":
							case "events":
								that.featuredImagesLauncher();
							break;
						}
					},
					singlePageAnimate: function(pageIDrequest){
						var that = this;
						that.animateSomething({
							target: $('body'),
							animation: {
								scrollTop: $('#page_' + pageIDrequest).offset().top - $('.pageSection').offset().top,
							},
							time: 400,
							callback: function() {
								console.log('page loaded')
							}
						})
				    }
				}
			});
		}

	});

})(jQuery);