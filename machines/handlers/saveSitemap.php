<?php
	// ini_set('display_errors', 1);
	// error_reporting(E_ALL);
	
	$sitemapDOM = $_POST['data'];
	$fileURL = '/var/www/html/sitemap.xml';

	$fp = fopen($fileURL, 'w');
	
	if ( $fp === false ) {
		echo print_r(error_get_last());
		exit();
	}

	$fileWritten = fwrite($fp, $sitemapDOM);
	fclose($fp);

	if ( $fileWritten === false ) {
		echo print_r(error_get_last());
	} else {
		echo "success";
	}
?>