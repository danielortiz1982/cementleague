<?php
	require_once realpath(dirname(__FILE__).'/../../../../..').'/wp-load.php';

	$formData = $_POST['formData'];
	$blogID = intval($_POST['blogID']);
	parse_str($formData, $formArray);

	$comment_content = $formArray['comment'];
	$comment_author_email = $formArray['email'];
	// echo json_encode(wp_get_current_user());

	$currentUser = wp_get_current_user();

	$commentdata = array(
		'comment_post_ID' => $blogID, 
		'comment_author' => $comment_author_email, 
		'comment_author_email' => $comment_author_email, 
		'comment_content' => $comment_content, 
		'comment_type' => '', 
		'comment_parent' => 0, 
		'user_id' => $currentUser, 
	);

	//Insert new comment and get the comment ID
	$comment_id = wp_new_comment( $commentdata );
	wp_set_comment_status( $comment_id, 'hold' );
	
	if(is_int($comment_id)){
		echo "success";
	} else {
		echo "failure";
	}

?>