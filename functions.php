<?php

// LOAD CONSTANTS
	$devDir = (isset($_GET["dev"])) ? get_template_directory_uri() . '-dev' : get_template_directory_uri();
	if(isset($_GET["dev"]) && $_GET["dev"] != ""){
		$devDir = get_template_directory_uri() . '-' . $_GET["dev"];	
	}
    define( 'PAGEDIR', $devDir );

	$tempDir = (isset($_GET["dev"])) ? get_template_directory() . '-dev' : get_template_directory();
	if(isset($_GET["dev"]) && $_GET["dev"] != ""){
		$tempDir = get_template_directory() . '-' . $_GET["dev"];	
	}
    define( 'TEMPDIR', $tempDir );

	$devMode = (isset($_GET["dev"])) ? true : false;
    define( 'DEVMODE', $devMode );

	require_once(TEMPDIR . '/machines/custom-functions.php');


?>