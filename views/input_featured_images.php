<!-- create php helper variables -->
	<?php
		global $post;
		$upload_link = esc_url( get_upload_iframe_src( 'image', $post->ID ) );
		$_js_you_have_img = ($inputValue == "") ? 'false' : 'true';
	?>

<!-- create DOM object for the controller -->
	<div class="featured_images_controller">
		<p class="hide-if-no-js">
			<a class="upload-custom-img" href="<?php echo $upload_link ?>">
				<?php _e('Add featured images') ?>
			</a>
		</p>
		<ul class="custom-img-container sortable"></ul>
		<input type="hidden" class="hidden_meta" id="<?php echo $inputID; ?>" name="<?php echo $inputID; ?>" value='<?php echo $inputValue; ?>' >
	</div>
	
<!-- set up javascript -->
	<script>

		jQuery(function($){

			function featuredImageSortable(target) {
				target.sortable();
				target.disableSelection();
				target.on( "sortstop", function( event, ui ) {
					updateImageOrder(target);
				});
			}

			function updateImageOrder(target) {
				var image_order_array = [];
				var image_order_string = '';
				target.children().each(function (argument) {
					if($(this).attr('data-imageid') != ""){
						image_order_array.push($(this).attr('data-imageid'))
					}
				});
				image_order_string = image_order_array.join();
				$('#<?php echo $inputID;?>').val(image_order_string);

			}

			function addImagesToContainer (imageArray, target) {
				var pageDir = '<?php echo PAGEDIR; ?>';
				$.post(pageDir + '/machines/handlers/loadPost.php', {postRequest: imageArray}, function(imageData){
					imageArray = _.filter(imageArray, function (argument) {
						return argument != "";
					})
					target.html('')
					_.each(imageArray, function (value_featuredImagesIds, index_featuredImagesIds) {
						var this_imageData = imageData[value_featuredImagesIds];
						var returnObject = $('<div class="featuredImageItem" />');
						var imageObject = $('<div class="featuredImageContainer" />');
						var deleteObject = $('<div class="delete displayNone"><div class="displayTable"><div class="displayTableCell">delete</div></div></div>');
						var thisImageURL = this_imageData.image_data.full;

						imageObject.css('background-image', 'url(' + thisImageURL + ')');

						returnObject.append(imageObject);
						returnObject.append(deleteObject);
						returnObject.attr('data-imageid', value_featuredImagesIds)

						target.append(returnObject);
					})
					featuredImageSortable(target);
					bindEvents(target);
				}, "json");
			}

			function bindEvents(target) {
				target.find('.delete').off('click')
				target.find('.delete').on('click', function (argument) {
					$(this).parents('.featuredImageItem').remove();
					updateImageOrder(target);
				})

				target.find('.featuredImageItem').off('mouseover')
				target.find('.featuredImageItem').off('mouseout')

				target.find('.featuredImageItem').on('mouseover', function (argument) {
					$(this).find('.delete').removeClass('displayNone')
				});
				target.find('.featuredImageItem').on('mouseout', function(){
					$(this).find('.delete').addClass('displayNone')
				});
			}

			$(document).ready(function(){

				// Set all variables to be used in this scope
					var frame;
					var metaBox = $('#<?php echo $inputID; ?>_meta.postbox');
					var addImgLink = metaBox.find('.upload-custom-img');
					var imgContainer = metaBox.find( '.custom-img-container');
					var input_container = $('#<?php echo $inputID;?>');
					var image_is_already_set = '<?php echo $_js_you_have_img; ?>';

				// LOAD SAVED IMAGES
					if(image_is_already_set){
						var featuredImagesIds = $('#<?php echo $inputID;?>').val().split(',');
						addImagesToContainer(featuredImagesIds, imgContainer);
					}

				// ADD IMAGE LINK
					addImgLink.on( 'click', function( event ){

						event.preventDefault();

						// If the media frame already exists, reopen it.
							if ( frame ) {
								frame.open();
								return;
							}

						// Create a new media frame
							frame = wp.media({
								title: 'Select Featured Images',
								button: {
									text: 'Set Featured Images'
								},
								multiple: true
							});


						// On media frame open
							frame.on( 'open', function() {
								var selection = frame.state().get('selection');
								var featuredImagesIds = $('#<?php echo $inputID;?>').val().split(',');
								var featuredImages = [];

								$.each(featuredImagesIds, function (index, val) {
									var attachment = wp.media.attachment(val);
									featuredImages.push(attachment)
								});

								selection.add(featuredImages)
							} );



						// On close image frame by clicking the blue submit button
							frame.on( 'select', function() {
								var these_attachments = frame.state().get('selection').toJSON();
								var these_ids = _.pluck(these_attachments, 'id').join();
								var featuredImagesIds = these_ids.split(',');
								addImagesToContainer(featuredImagesIds, imgContainer);
								input_container.val(these_ids);
							});

						// Bind modal click event
							frame.open();
					});

			})


		});

	</script>

<!-- set up styles -->
	<style>
		.featuredImageItem{
			position: relative;
			width: 100%;
			height: 100px;
			margin-bottom: 20px;
		}
		.featuredImageItem:hover{
			cursor: move;
		}
		.featuredImageContainer{
			position: absolute;
			top: 0px;
			left: 0px;
			height: 100%;
			width: 100%;
			z-index: 100;
			background-position: center;			
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
		}
		.delete{
			position: absolute;
			top: 0px;
			left: 0px;
			height: 25%;
			width: 100%;
			z-index: 101;
			background-color: rgba(255, 0, 0, 0.5);
			text-align: center;
			color: white;
			font-weight: bold;
			text-transform: uppercase;
		}
		.delete:hover{
			cursor: pointer;
		}
		.displayTable{
			display: table;
			height: 100%;
			width: 100%;
		}
		.displayTableCell{
			display: table-cell;
			vertical-align: middle;
		}
		.displayNone{
			display: none;
		}

	</style>