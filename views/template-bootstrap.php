<!DOCTYPE html>
<html lang="en" class="<?php echo returnBrowser(); ?>">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title><?php bloginfo('name');?></title>

        <?php wp_head(); ?>

        <link rel='stylesheet' type='text/css' href='<?php echo PAGEDIR; ?>/machines/libraries/mmenu/jquery.mmenu.all.css' />
        <link rel="stylesheet" type="text/css" href="<?php echo PAGEDIR; ?>/machines/libraries/shadowbox/shadowbox.css">
        <link rel="stylesheet" type="text/css" href="<?php echo PAGEDIR; ?>/styles/styles.min.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body data-tempdir="<?php echo PAGEDIR; ?>" id="<?php echo get_post( $post )->post_name; ?>" <?=(returnMobile() == 'true')? 'class="mobileMode"' : 'class="desktopMode"';?>>

        <!-- POPULATE JAVASCRIPT VARIABLES -->
            <div>
                <?php 
                    generatePagesJSON(get_the_ID());
                    populateJavascript(returnMobile(), 'mobile');

                    if(DEVMODE){
                        populateJavascript('true', 'dev_mode');
                    } else {
                        populateJavascript('false', 'dev_mode');
                    }

                    if(isset($_GET['url'])){
                        populateJavascript('true', 'dev_refresh');
                    } else {
                        populateJavascript('false', 'dev_refresh');
                    }

                    if ( is_user_logged_in() ) {
                        populateJavascript('true', 'user_logged_in');

                        $userdat = wp_get_current_user();
                        $allUserData = listUsers('array');

                        foreach ($allUserData as $key => $value) {
                            if($value['id'] == $userdat->data->ID){
                                $userData = $value;
                            }
                        }

                        populateJSON($userData, 'user_data');
                    } else {
                        populateJavascript('false', 'user_logged_in');
                    }            
                ?>
            </div>

        <!-- HEADER -->
        <nav class="navbar navbar-default navbar-fixed-top header">
            <div class="container">
                <div class="navbar-header">

                    <div class="menu_toggle_container">

                        <div class="displayTable">
                            <div class="displayTableCell">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                        </div>

                    </div>
                    
                    <div class="logo_container">
                        <div class="displayTable">
                            <div class="displayTableCell">
                                <a class="navbar-brand" href="<?php echo home_url(); ?>">
                                    <img src="<?php echo PAGEDIR; ?>/images/graphics/logo.png" alt="<?php bloginfo('name');?>" />
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
                <?php 

                class bootstrapDropDown extends Walker_Nav_Menu {
                    function start_lvl( &$output, $depth = 0, $args = array() ) {
                        $indent = str_repeat("\t", $depth);
                        $output .= "\n$indent<ul class=\"sub-menu dropdown-menu\">\n";
                    }
                }

                $defaults = array(
                    'theme_location'  => 'header-menu',
                    'menu'            => '',
                    'container'       => 'div',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id'    => 'navbar',
                    'menu_class'      => 'nav navbar-nav',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '<span>',
                    'link_after'      => '</span>',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth'           => 0,
                    'walker'          => new bootstrapDropDown()
                    );
                wp_nav_menu( $defaults );
                ?>
            </div>
        </nav>

        <!-- PAGE CONTENT -->
            <section class="bannerContainer"></section>               

            <div class="title_block">
                <div class="container">
                <div class="displayTable">
                    <div class="displayTableCell">
                        <h2 class="the_title header_title"></h2>
                    </div>
                </div>
                </div>
            </div>
        <!-- PAGE CONTENT -->
            <section class="pageSection"></section>

        <!-- FOOTER -->
        <footer class="footer">
            <div class="container">
                <div class="footerBox">
                    <div class="footerBoxLeft">
                        <div class="displayTable">
                            <div class="displayTableCell">
                                <p>
                                    The Cement League<br />
                                    49 West 45th Street, 9th Floor<br />
                                    New York, NY 10036<br />
                                    T: 212.575.0950 | F: 212.575.4844 | <a href="mailto:thecementleague@verizon.net">thecementleague@verizon.net</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="footerBoxRight">
                        <div class="displayTable">
                            <div class="displayTableCell">
                                <p>
                                    <a href="http://www.bermangrp.com" target="_blank" class="hyperlink">Website Design by The Berman Group</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
            
        <!-- SCRIPTS -->
            <div>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/shadowbox/shadowbox.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/modernizr/modernizr.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/backstretch/backstretch.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/boilerplate/boilerplate.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/mmenu/jquery.mmenu.min.all.js"></script>
                <?php if (returnBrowser() !== "internet-explorer-8"): ?>
                    <script src="<?php echo PAGEDIR; ?>/machines/libraries/dynamics/dynamics.js"></script>
                <?php endif; ?>
                <?php wp_footer(); ?>
            </div>
    </body>
</html>