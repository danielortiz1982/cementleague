<div class="unionItemContainer">
	<div class="union_item">
		<div class="union_info_block">
			<h4 class="the_title"></h4>
			<p>
				<span class="union_address_1"></span><br />
				<span><span class="union_city"></span>, <span class="union_state"></span> <span class="union_zip_code"></span></span><br />
				<span><span>Phone: </span><span class="union_phone"></span></span><br />
				<span><span>Fax: </span><span class="union_fax"></span></span>
			</p>
		</div>
		<div class="union_rates_block">
			<h5>Rates</h5>
			<div class="union_rates"></div>
		</div>
	</div>
</div>